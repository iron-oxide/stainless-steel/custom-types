//! Rectangles
//!
//! A toy for playing with rust structs.

#[derive(Debug)]
struct Rectangle {
    length: u32,
    width: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.length * self.width
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.length > other.length && self.width > other.width
    }

    /// Associated function used as a constructor
    fn square(size: u32) -> Rectangle {
        Rectangle { length: size, width: size }
    }
}

fn main() {

    let rect1 = Rectangle { length: 50, width: 30 };
    println!("rect1 is {:?}\n", rect1);
    println!("rect1 is {:#?}\n", rect1);

    println!(
        "The area of the rectangle is {} square pixels\n",
        rect1.area()
    );

    let rect2 = Rectangle { length: 40, width: 10 };
    let rect3 = Rectangle { length: 45, width: 60 };
    println!("Can rect1 hold rect2? {}", rect1.can_hold(&rect2));
    println!("Can rect1 hold rect3? {}\n", rect1.can_hold(&rect3));

    let square = Rectangle::square(42);
    println!("square is {:#?}\n", square);
}
