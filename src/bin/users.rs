//! users
//!
//! A toy to play with rust structs.

/// A user of the application
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn main() {

    info("Creating user1");
    let user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };

    info("Creating user2 with 'field int shorthand'");
    let name = String::from("someone");
    let email_address = String::from("otherperson@example.com");
    let user2 = build_user(name, email_address);

    /// Struct update syntax lets values from other
    /// intances be used in a much more concice manner
    /// than manually copying everything.
    info("Creating user3 with struct update syntax");
    let user3 = User {
        email: String::from("myotheremail@otherplace.com"),
        ..user2
    };

    /// Tuple structs are a thing too.
    /// They have no named fields.
    info("Creating tuple structs");
    struct Color(i32, i32, i32); // (r, g, b)
    struct Point(i32, i32, i32); // (x, y, z)

}

/// Build a user with defaults
///
/// Uses field int shorthand to be more concise.
/// If the parameter names are the same as the
/// struct's field names they can be left out.
///
fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}

/// Info message formatting
fn info(message: &str) {
    println!("[*] {}", message);
}
