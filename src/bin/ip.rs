//! ip
//!
//! An enum toy to play with.

/// Can be just values or have attached data
enum IpAddr {
    v4(u8, u8, u8, u8),
    v6(String),
}

fn main() {
    let home = IpAddr::v4(127, 0, 0, 1);
    let loopback = IpAddr::v6(String::from("::1"));
}
