//! options
//!
//! The rust Option<T> type.

fn main() {

    // Can infer the Option type
    let some_number = Some(5);
    let some_string = Some("a string");

    // Can not infer type from None
    // it must be provided
    let absent_number: Option<i32> = None;

    let x = 5;

    /// some_number must be converted from type `Option<T>`
    /// to `T` before it can be used like `T`. This is because
    /// `Option<T>` is an enum and not `T`.
    let y = some_number.unwrap_or(10);
    let z = absent_number.unwrap_or(10);

    println!("x + y = {}", x + y);
    println!("x + z = {}", x + z);

}
